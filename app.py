from flask import Flask, render_template, request,jsonify
import os
import sys
from json_encoder import json
from flask_cors import CORS
from firebase_admin import credentials, firestore, initialize_app
import requests
import re
import random
import logging
import pickle
import os.path
from oauth2client.service_account import ServiceAccountCredentials
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from linebot import (
    LineBotApi, WebhookHandler
)
from linebot.exceptions import (
    InvalidSignatureError
)
from linebot.models import (
    MessageEvent, PostbackEvent, TextMessage, TextSendMessage, FlexSendMessage,
    SourceUser, ButtonsTemplate, PostbackAction, TemplateSendMessage,
    BubbleContainer, URIAction, ImageComponent, BoxComponent,
    ButtonComponent, SeparatorComponent,
)


app = Flask(__name__)
CORS(app, resources={r"/*": {"origins": "*"}})



lineoa = '536huism'


cred = credentials.Certificate('multicaster-95642410e3c0.json')
default_app = initialize_app(cred)
db = firestore.client() 
caster = db.collection(u'caster').document(lineoa).get().to_dict()
line_bot_api = LineBotApi(caster['access'])
handler = WebhookHandler(caster['secret'])


@app.route("/")
def index():
  return "Hello World!"

@app.route("/callback", methods=['POST'])
def callback():
    # get X-Line-Signature header value
    signature = request.headers['X-Line-Signature']

    # get request body as text
    body = request.get_data(as_text=True)
    app.logger.info("Request body: " + body)

    # handle webhook body
    try:
        handler.handle(body, signature)
    except InvalidSignatureError:
        print("Invalid signature. Please check your channel access token/channel secret.")
        abort(400)

    return 'OK'


@handler.add(MessageEvent, message=TextMessage)
def handle_message(event):
  
  text = event.message.text
  line_user_id = event.source.user_id
  inform = db.collection(caster['customerdb'])


  
  if hasattr(event.source, 'group_id'):
    informGroup = inform.document(event.source.group_id)
    informGroupRef = informGroup.get()
    if not informGroupRef.exists :
      informGroupDict = dict()
      informGroupDict['uuid'] = event.source.group_id
      informGroupDict['type'] = 'group'
      informGroup.set(informGroupDict)
      
  
  
  informPerson = inform.document(line_user_id)
  informPersonRef = informPerson.get()
  if not informPersonRef.exists :
    informPersonDict = dict()
    informPersonDict['uuid'] = line_user_id
    informPersonDict['type'] = 'user'
    informPerson.set(informPersonDict)
  cmd = text.split('|')

  if u'follower' in cmd[0]:
    personToInform = inform.stream()
    person_list = []
    for person in personToInform:
      if 'uuid' in person.to_dict() and person.to_dict()['type'] == 'user':
        person_list += [person.to_dict()['uuid']]
    line_bot_api.multicast(person_list, my_flex())
    
  if u'group' in cmd[0]:
    groupToInform = inform.stream()
    for group in groupToInform:
      if 'uuid' in group.to_dict() and group.to_dict()['type'] == 'group':
        line_bot_api.push_message(group.to_dict()['uuid'], my_flex())
        
  if u'broadcast' in cmd[0]:
    line_bot_api.broadcast(my_flex())
    
  elif u'เบื่อจัง' in text:
    line_bot_api.reply_message(event.reply_token, my_flex())
  

def my_flex():
  return FlexSendMessage(
    alt_text=caster['message'],
    contents=BubbleContainer(
        direction='ltr',
        hero=ImageComponent(
            url=caster['imgurl'],
            size='full',
            aspect_ratio=caster['ratio'],
            aspect_mode='cover',
            action=URIAction(uri=caster['web'], label='หน้าเว็บ')
        ),
        footer=BoxComponent(
                  layout='horizontal',
                  spacing='sm',
                  contents=[
                      ButtonComponent(
                          style='link',
                          height='sm',
                          action=URIAction(uri=caster['targetline'], label='สมัครเลย')
                          ),
                      # separator
                      SeparatorComponent(),
                      # websiteAction
                      ButtonComponent(
                          style='link',
                          height='sm',
                          action=URIAction(uri=caster['liff'], label='แชร์เลย')
                          )
                  ])
                )
              )

port = int(os.environ.get('PORT', 5000))
if __name__ == '__main__':
    app.run(threaded=True, host='0.0.0.0', port=port) 
