FROM python:3.5.2-slim
RUN apt-get update -y
RUN apt-get install -y python-pip python-dev build-essential libffi-dev

# RUN apt-get update -y && apt-get install -y wget curl unzip libgconf-2-4
# RUN apt-get update -y && apt-get install -y chromium xvfb python3 python3-pip 
# RUN wget -O /tmp/chromedriver.zip http://chromedriver.storage.googleapis.com/`curl -sS chromedriver.storage.googleapis.com/LATEST_RELEASE`/chromedriver_linux64.zip
# RUN unzip /tmp/chromedriver.zip chromedriver -d /usr/local/bin/

# install selenium
RUN pip install --upgrade pip
# RUN pip install selenium


COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt
CMD ["python", "app.py"]